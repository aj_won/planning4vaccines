import sys

""" Generates the problem file (agents, initial state and goal).
"""
class lote(object):
    def __init__(self,name,type):
        self.name = name
        self.type = type
        self.on_coord = False
    
    def set_coordinate(self,coord):
        self.coordinate = coord

    def set_camara(self,cam):
        self.camara = cam

    def set_on_coord(self,i):
        self.on_coord = i
    
    def set_validade(self, data):#data no formato MMDD
        self.validade = data

class coordinate(object):
    def __init__(self,name,camara):
        self.name = name
        self.camara = camara
        self.updated = False
        self.validade = 10000
    
    def update(self,status,type):
        self.updated = True
        self.type = type #tipo da vacina armazenada
        self.status = status #full, empty, half_full

    def set_validade_min(self, data):
        if int(data) < self.validade:
            self.validade = data
    
    def reset(self):
        self.updated = False
        self.validade = 10000
        self.type = ''
        self.status = ''

class camara(object):
    def __init__(self,name,types):
        self.name = name
        self.types = types
        self.coords = []

    def add_coords(self,c):
        self.coords += c 


def generate_lotes(n_lotes,type,validade, begin):
    lotes = []
    for l in range(begin, begin+n_lotes):
        name = 'lote' + str(l)
        lo = lote(name, type)
        lo.set_validade(validade)
        lotes.append(lo)
    return lotes

def generate_coords(n_coord,camara, begin):
    coords = []
    #string =''
    for c in range(begin,n_coord + begin):
        name = 'coord' + str(c)
        #string += ('\t\t' + '(empty ' + name + ')\n')
        #string += ('\t\t' + '(coord_in_camara ' + name +' ' + camara.name + ')\n')
        coords.append(coordinate(name,camara))
    return coords

def generate_camara(name, types, coord_begin,n_coord):
    c = camara(name, types)
    coords = generate_coords(n_coord,c,coord_begin)
    c.add_coords(coords)
        
    return c


def print_problem_file(lotes,camaras, coordenadas, file):
    file.write(';; Lotes problem - PDDL problem file\n')
    #file.write(';; Lotes ' + str(depth()) + ', ' + str(nb_agts()) + ' agents\n\n')

    file.write('(define (problem Lotes1)\n')
    file.write('\t(:domain camaras_frias)\n\n')

    file.write('\t(:objects ' + '\n\t\t')#OBJECTS

    for i in lotes: #LOTES -----------------------------------------------------------
        if i.on_coord == False:
            file.write(str(i.name) + ' ')

    file.write(' - lote\n\t\t' )

    for i in camaras: #CAMARAS----------------------------------------------------------
        file.write('camara' + str(i.name) + ' ')

    file.write(' - camara\n\t\t')

    for i in coordenadas: #COORDENADA----------------------------------------------------------
        file.write(str(i.name) + ' ')

    file.write(' - coordenada\n' + '\t)\n\n')

    file.write('\t(:init\n')#INIT
    for i in lotes:#LOTES -----------------------------------------------------------
        if i.on_coord == False:
            file.write('\t\t' + '(is_type '+ str(i.name) + ' '+ str(i.type) +')\n')

    for i in camaras:#CAMARAS -----------------------------------------------------------
        for t in i.types:
            file.write('\t\t' + '(accepts_type '+ 'camara' + str(i.name) + ' '+ t +')\n')

    for i in coordenadas: #COORDENADAS----------------------------------------------------------
        if i.updated == True:
            if i.status != 'empty':
                file.write('\t\t' + '(' + str(i.status) +' '+ str(i.name) +  ')\n')
                file.write('\t\t' + '(has_type ' + str(i.name) +' '+ str(i.type) + ')\n')
        else:
            file.write('\t\t' + '(empty ' + str(i.name) +  ')\n')
        file.write('\t\t' + '(coord_in_camara ' + str(i.name) +' '+ 'camara' + str(i.camara.name) +  ')\n')

    file.write('\t)\n\n')

    file.write('\t(:goal (and\n')#GOAL
    for i in lotes:
        if i.on_coord == False:
            file.write('\t\t' + '(on_coordenada ' + str(i.name) +')\n')

    file.write('\t)\n')
    file.write(')\n')
    file.write(')\n')


def read_plan(file,coordenadas,lotes):
    with open(file,'r') as f:
        lines = f.readlines()
        for coord in coordenadas:
            for line in lines:
                index = line.find(coord.name)
                if index != -1 :
                    parse = line.split(' ')
                    if len(parse) == 6:
                        lote_index = int(parse[2][4:])
                        lotes[lote_index].set_on_coord(True)
                        coord.set_validade_min(lotes[lote_index].validade)
                        if parse[1].find('empty') != -1:
                            coord.update('full',parse[4])
                        else:
                            coord.update('half_full',parse[4])


lotes = []
camaras = []
coordenadas = []
ans = ''

# Reading the command line arguments
while ans != '0':#generating camadas e coordenadas
        n_camaras = int(input("Numero de camaras: "))
        camara_types = []
        n_types = int(input ("Numero de tipos da camara: "))
        for i in range(n_types):
            camara_types.append( input("Camara aceita tipo: "))
        
        n_coord = int(input("Numero de coordenadas da camara: "))
        ans = input("Precione 0 para continuar ou 1 para adicionar mais camaras: ")
        
        for i in range(n_camaras):
            cam = generate_camara(len(camaras), camara_types,len(coordenadas),n_coord)
            camaras.append(cam)
            coordenadas += cam.coords

while ans != '3':
    ans = ''
    ans = input("Precione 0 para adicionar lotes, Precione 1 para ler o plano, Precione 2 para retirar lotes: ")

    if ans == '0':
        ans = ''
        while ans != '0':
            n_lotes = int(input("Numero de lotes: "))
            lotes_type = input("Tipo do carregamento: ")
            validade = int(input("Validade do carregamento: "))
            ans = input("Precione 1 para adicionar mais lotes ou 0 para continuar: ")
            lotes += generate_lotes(n_lotes,lotes_type,validade,len(lotes))
      

        print('Writing problem...')
        problem_file = open('ProblemPy.pddl','w')
        print_problem_file(lotes,camaras,coordenadas,problem_file)
        problem_file.close()
    elif ans == '1':
        read_plan('ProblemPy.plan',coordenadas,lotes)
    elif ans == '2':
        n_send = int(input("Numero de lotes para enviar: "))
        send = []
        sort = sorted(coordenadas, key= lambda coordinate: coordinate.validade)
        for i in sort:
            send.append(i)
            print("Enviar lotes " + i.type + " da coordenada " + i.name + " da camara "+ str(i.camara.name) + " com validade " + str(i.validade))
            i.reset()
                
            if len(send) == n_send:
                break
            


