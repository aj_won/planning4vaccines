;; Lotes problem - PDDL problem file
(define (problem Lotes1)
	(:domain camaras_frias)

	(:objects 
		lote0 lote1 lote2  - lote
		camara0  - camara
		coord0 coord1 coord2 coord3 coord4  - coordenada
	)

	(:init
		(is_type lote0 Pfizer)
		(is_type lote1 Pfizer)
		(is_type lote2 Pfizer)
		(accepts_type camara0 Pfizer)
		(accepts_type camara0 Coronavac)
		(empty coord0)
		(coord_in_camara coord0 camara0)
		(empty coord1)
		(coord_in_camara coord1 camara0)
		(empty coord2)
		(coord_in_camara coord2 camara0)
		(empty coord3)
		(coord_in_camara coord3 camara0)
		(empty coord4)
		(coord_in_camara coord4 camara0)
	)

	(:goal (and
		(on_coordenada lote0)
		(on_coordenada lote1)
		(on_coordenada lote2)
	)
)
)
