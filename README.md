# Planning4Vaccines

Python project made on visual studio code. Requires PDDL extension.

Projeto desenvolvido para a matéria de Sistemas Inteligentes na UTFPR. 2021.

Para rodar o programa, deve-se rodar o arquivo .py no terminal.

Depois de preencher as informações um arquivo ProblemPy.pddl será gerado, e junto com o arquivo Domain.pddl, devem ser executados em no planner http://editor.planning.domains/#.

O arquivo .plan pode ser gerado pelo Planner e quando lido pelo programa pode gerar estados iniciais diferente para o problema.
