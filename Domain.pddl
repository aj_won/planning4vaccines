;Header and description

(define (domain camaras_frias)

;remove requirements that are not needed
(:requirements :strips :typing :conditional-effects :negative-preconditions )

(:types 
    camara lotetype coordenada lote temperatura
)

; un-comment following line if constants are needed
(:constants 
    Pfizer
    AZ 
    Moderna
    Coronavac - lotetype
)

(:predicates ;todo: define predicates here

    ;; predicados do lote
    (on_coordenada ?l - lote) ;; o lote esta em sua coordenada

    (is_type ?l - lote ?t - lotetype);; lote l has type t

    ;;predicados da coordenata
    (full ?c - coordenada) ;; a coordenada tem espaço

    (empty ?c - coordenada) 

    (half_full ?c - coordenada) ;; a coordenada tem metade da capacidade

    (has_type ?c - coordenada ?t - lotetype );; coordenada possui lote deste tipo?

    (coord_in_camara ?c - coordenada ?ca - camara);coordenada esta na camara

    (accepts_type ?ca - camara ?t - lotetype) ;camara aceita tipo

    (lote_in_camara ?l - lote ?ca - camara) ;lote in camara

    
)



;define actions here
(:action take_to_coord_empty ;; carrega lote  até a coordenada certa vazia
    :parameters (?l - lote ?c - coordenada ?t - lotetype ?ca - camara)

    :precondition (and 
                    (empty ?c)
                    (is_type ?l ?t)
                    (coord_in_camara ?c ?ca)
                    (lote_in_camara ?l ?ca) )
    :effect (and
                (on_coordenada ?l)
                (half_full ?c)
                (has_type ?c ?t)
                (not (empty ?c)) )
)

(:action take_to_camara ;; carrega lote até a camara
    :parameters (?l - lote ?ca - camara ?t - lotetype)

    :precondition (and 
                    (is_type ?l ?t)
                    (accepts_type ?ca ?t) )
    :effect (and
                (lote_in_camara ?l ?ca) )
)

(:action take_lote_to_coord_half_full ;; carrega lote  até a coordenada certa half_full
    :parameters (?l - lote ?c - coordenada ?t - lotetype ?ca - camara)

    :precondition (and 
                    (not(empty ?c))
                    (not(full ?c))
                    (is_type ?l ?t)
                    (has_type ?c ?t)
                    (coord_in_camara ?c ?ca)
                    (lote_in_camara ?l ?ca) )
    :effect (and
                (on_coordenada ?l)
                (full ?c)
                (not (half_full ?c))
                (has_type ?c ?t) )
)

(:action empty_a_coordinate ;; esvazia um coordenada
    :parameters ( ?c - coordenada)

    :precondition (and 
                    (not(empty ?c)  )
                    )
    :effect (and
                (empty ?c)
                (not (half_full ?c))
                (not(full ?c))
                (not(has_type ?c Pfizer)) )
)

)